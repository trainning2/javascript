$(document).ready(() => {
  // Java script
  // const login = document.querySelector(".login-button");
  // const loginMenu = document.querySelector(".login-form");

  // login.addEventListener("click", () => {
  // if (loginMenu.style.display === "none") {
  //     loginMenu.style.display = "block";
  // } else {
  //     loginMenu.style.display = "none";
  // }
  // });

  jQuery;
  $(".login-button").on("click", () => {
    $(".login-form").toggle();
  });

  const $menuButton = $(".menu-button");
  const $navDropdown = $("#nav-dropdown");

  $menuButton.on("click", () => {
    $navDropdown.show();
  });

  $navDropdown.on("mouseleave", () => {
    $navDropdown.hide();
  });
});
